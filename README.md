# Restful API with NodeJS, Express, PostgreSQL and Sequelize

## 💻 Technologies

- ⚡ Express — A web framework for Node.js
- 💾 Sequelize — SQL dialect ORM for Node.js
- 🐘 PostgreSQL — An open source object-relational database

## ⛔ Prerequisites

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/docs/install)

## 🔌 Installation

1. Clone this repository;
2. Alter the credentials in `/src/config/database.js`;
3. Run `yarn` to install dependencies;
4. Run `yarn sequelize db:create` to create database;
5. Run `yarn sequelize db:migrate` to execute migrations;
6. Run `yarn dev` to start server;
