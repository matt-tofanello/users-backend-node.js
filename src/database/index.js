const Sequelize = require("sequelize");
const dbConfig = require("../config/database");

const User = require("../models/User");
const Occupation = require("../models/Occupation");

const connection = new Sequelize(dbConfig);

User.init(connection);
Occupation.init(connection);

User.associate(connection.models);
Occupation.associate(connection.models);

module.exports = connection;
