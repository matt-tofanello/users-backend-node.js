const express = require("express");
const routes = require("./routes");
const cors = require('cors');

require("./database");

const app = express();

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

app.options('*', cors());

app.use(express.json());
app.use(routes);

app.listen(process.env.PORT || 3333);
