const express = require('express');

const UserController = require('./controllers/UserController');
const OccupationController = require('./controllers/OccupationController');

const routes = express.Router();

routes.get('/users', UserController.read);
routes.get('/users/:id', UserController.readById);
routes.post('/users', UserController.create);
routes.put('/users/:id', UserController.update);
routes.delete('/users/:id', UserController.delete);

routes.get('/users/:user_id/occupations', UserController.readOccupations);
routes.post('/users/:user_id/occupations', UserController.createOccupation);
routes.delete('/users/:user_id/occupations', UserController.deleteOccupation);

routes.get('/occupations', OccupationController.read);
routes.delete('/occupations', OccupationController.delete);

module.exports = routes;
