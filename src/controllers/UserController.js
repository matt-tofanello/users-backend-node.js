const User = require("../models/User");
const Occupation = require("../models/Occupation");

module.exports = {
  async read(req, res) {
    const users = await User.findAll({
      attributes: ["id", "cpf", "name", "surname", "email", "cellphone", "birthday"],
      include: {
        association: "occupations",
        attributes: ["name"],
        through: {
          attributes: []
        }
      }
    });

    return res.json(users);
  },

  async readById(req, res) {
    const { id } = req.params;
    const user = await User.findOne({
      attributes: ["cpf", "name", "surname", "email", "cellphone", "birthday"],
      include: {
        association: "occupations",
        attributes: ["name"],
        through: {
          attributes: []
        }
      },
      where: { id }
    });

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    return res.json(user);
  },

  async create(req, res) {
    const { cpf, name, surname, email, cellphone, birthday } = req.body;

    const cpfAlreadyExists = await User.findOne({
      where: { cpf }
    });

    if (cpfAlreadyExists) {
      return res.status(400).json({ error: "CPF already exists" });
    }

    const user = await User.create({ cpf, name, surname, email, cellphone, birthday });

    return res.json(user);
  },
  
  async update(req, res) {
    const { id } = req.params;
    const { cpf, name, surname, email, cellphone, birthday } = req.body;

    const user = await User.findByPk(id);

    if (!user) {
      return res.status(400).json({ error: "User not found" });
    }

    const userUpdated = await user.update({ cpf, name, surname, email, cellphone, birthday });

    return res.json(userUpdated);
  },

  async delete(req, res) {
    const { id } = req.params;

    const user = await User.findByPk(id);

    if (!user) {
      return res.status(400).json({ error: "User not found" });
    }

    await user.destroy();

    return res.json();
  },

  async readOccupations(req, res) {
    const { user_id } = req.params;

    const user = await User.findByPk(user_id, {
      include: {
        association: "occupations",
        attributes: ["name"],
        through: {
          attributes: ["user_id", "occupation_id"]
        }
      }
    });

    if (!user) {
      return res.status(400).json({ error: "User not found" });
    }

    return res.json(user.occupations);
  },

  async createOccupation(req, res) {
    const { user_id } = req.params;
    const { name } = req.body;

    const user = await User.findByPk(user_id);

    if (!user) {
      return res.status(400).json({ error: "User not found" });
    }

    const [occupation] = await Occupation.findOrCreate({
      where: { name }
    });

    await user.addOccupation(occupation);

    return res.json(occupation);
  },

  async deleteOccupation(req, res) {
    const { user_id } = req.params;
    const { name } = req.body;

    const user = await User.findByPk(user_id);

    if (!user) {
      return res.status(400).json({ error: "User not found" });
    }

    const occupation = await Occupation.findOne({
      where: { name }
    });

    await user.removeOccupation(occupation);

    return res.json();
  }
};
