const Occupation = require("../models/Occupation");

module.exports = {
  async read(req, res) {
    const occupations = await Occupation.findAll({
      attributes: ["name"],
      include: {
        association: "users",
        attributes: ["id", "cpf", "name", "surname", "email", "cellphone", "birthday"],
        through: {
          attributes: []
        }
      }
    });

    return res.json(occupations);
  },

  async delete(req, res) {
    const { name } = req.body;

    const occupation = await Occupation.findOne({
      where: { name }
    });

    if (!occupation) {
      return res.status(400).json({ error: "Occupation not found" });
    }

    await occupation.destroy();

    return res.json();
  }
};
