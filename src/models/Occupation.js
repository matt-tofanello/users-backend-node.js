const { Model, DataTypes } = require("sequelize");

class Occupation extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING
      },
      {
        sequelize,
        tableName: "occupations"
      }
    );
  }

  static associate(models) {
    this.belongsToMany(models.User, {
      foreignKey: "occupation_id",
      through: "user_occupations",
      as: "users"
    });
  }
}

module.exports = Occupation;
