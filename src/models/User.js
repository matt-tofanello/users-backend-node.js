const { Model, DataTypes } = require("sequelize");

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        cpf: DataTypes.STRING,
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        email: DataTypes.STRING,
        cellphone: DataTypes.STRING,
        birthday: DataTypes.DATEONLY
      },
      {
        sequelize
      }
    );
  }

  static associate(models) {
    this.belongsToMany(models.Occupation, {
      foreignKey: "user_id",
      through: "user_occupations",
      as: "occupations"
    });
  }
}

module.exports = User;
